## [1.3.1](https://gitlab.com/m.ragab6010/gitops-web-app-example/compare/v1.3.0...v1.3.1) (2023-06-17)


### Bug Fixes

* **gitlab-ci:** fixed gitlab ci file ([b02cf20](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/b02cf20b76d20886465e93a1d9a3ad61b6a4a9ca))

# [1.3.0](https://gitlab.com/m.ragab6010/gitops-web-app-example/compare/v1.2.0...v1.3.0) (2023-06-17)


### Features

* **tests:** add more tests ([8055c52](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/8055c52fd8552b9ea040f399c0d72a3e5a676e76))
* **tests:** add more tests ([991ac76](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/991ac7684934e00392ea392112ee712b9205c1e4))
* **tests:** add more tests ([c7fb5ca](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/c7fb5caf304da8a50865e0cc384bc598c5145f35))
* **tests:** add more tests ([91e48b1](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/91e48b125d088e52c11a18f24a82848db01ba422))
* **tests:** add more tests ([9e29810](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/9e2981082b026be859f96fab01511787a6078cd9))
* **tests:** add more tests ([2902c26](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/2902c26c4d8244d3e3b0a82d8194f5bda2de760e))
* **tests:** add more tests ([cafb73a](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/cafb73aa163e1c07cab783deb26451e4320e0d5c))
* **tests:** add more tests ([d984a1c](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/d984a1cce9c5b4be9f73be459d03f047a1ebe345))
* **tests:** add more tests ([e7b809a](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/e7b809af2b9d9e7f9f69f432654617e022d57db5))

# [1.2.0](https://gitlab.com/m.ragab6010/gitops-web-app-example/compare/v1.1.0...v1.2.0) (2023-06-17)


### Features

* **tests:** add more tests ([e3e0046](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/e3e0046d532c5cae43b173ed9019781813e05418))
* **tests:** add more tests ([7c19ea0](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/7c19ea07a319fe269bf9c2547a49e9d4f0693073))

# [1.1.0](https://gitlab.com/m.ragab6010/gitops-web-app-example/compare/v1.0.2...v1.1.0) (2023-06-17)


### Features

* **tests:** add more tests ([7f894f2](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/7f894f2783504b86b57ef9c408b9d9ecfa83e461))
* **tests:** add more tests ([5b37a94](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/5b37a9417e57fccc0e628412fb8a381de78846c9))

## [1.0.2](https://gitlab.com/m.ragab6010/gitops-web-app-example/compare/v1.0.1...v1.0.2) (2023-06-17)


### Bug Fixes

* **ci:** update ci file ([abe1746](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/abe1746193105a236e0bf517886ac1abb725a45e))
* **ci:** update ci file ([7942a12](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/7942a122991d11d5ec4b0f2a7f2aeb9f67a39c14))
* **ci:** update ci file ([446d8ff](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/446d8ff64d4f64e0c475f407df20bfac9d46ebdf))

## [1.0.1](https://gitlab.com/m.ragab6010/gitops-web-app-example/compare/v1.0.0...v1.0.1) (2023-06-17)


### Bug Fixes

* **ci:** update ci file ([f531d56](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/f531d5626c2caff91cebfc0914b568745abbd10c))
* **ci:** update ci file ([781067b](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/781067b8eed86d20889516d0906977f5a5993156))
* **ci:** update ci file ([b105dad](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/b105dadcb1a1289170860044351c408150402434))
* **ci:** update ci file ([ca91bde](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/ca91bde752f55f3454355efed066f957c6a5e588))
* **ci:** update ci file ([fb1d086](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/fb1d0866a3377176a79264a207f888d99073794d))
* **ci:** update ci file ([3e95783](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/3e95783f91b4a229bf91ba4e8a9ad8beb6c90453))
* **ci:** update ci file ([eec8771](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/eec8771214be4060b6f9a3ee285022ebe7cba6ec))
* **ci:** update ci file ([ae6192f](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/ae6192f691d9d1d75e763fd104f52cde31f2173e))
* **ci:** update ci file ([e0e56cf](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/e0e56cf9c6a0a3fa23e21bd44413db70578c7856))
* **ci:** update ci file ([7dcc39e](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/7dcc39efb7537346911877dd767c9dbd12c9e23d))
* **ci:** update ci file ([8547ea6](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/8547ea6de23cf9ca6be15c504c42cb53a7d996c5))
* **ci:** update ci file ([b5319f1](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/b5319f1e3cfb0bc19f39151dd47fca7ea30c2aae))
* **ci:** update ci file ([32bb144](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/32bb144971681861598ce373717033df967063f9))
* **ci:** update ci file ([2f36109](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/2f3610974046cbc08f3c67ea1a832c92fcca8d4f))
* **ci:** update ci file ([eae52d4](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/eae52d4139c3874cdb6d54768f842763461de42d))
* **ci:** update ci file ([54a64fe](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/54a64fecc216a0bf1ac4874fd46d9f84266fee87))
* **ci:** update ci file ([948cc7c](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/948cc7c76a870aeaab9aafb8d23645d75cef210f))
* **ci:** update ci file ([fb7d6e8](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/fb7d6e8d7859a988bf9da74a9274a6db8ea02604))
* **ci:** update ci file ([73ad391](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/73ad391e9052035ce3e6955705d57c3f3d155843))
* **ci:** update ci file ([7eaf04d](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/7eaf04d59a7adb7e0bfbe2127d830c10b78bc99a))

# 1.0.0 (2023-06-17)


### Bug Fixes

* **ci:** update ci file ([9f636bd](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/9f636bdfdbaa85c72a288a373be89251202784f6))
* **ci:** update ci file ([b9605f1](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/b9605f1167ff581950de6e4d15c4047f1b14437d))
* **ci:** update ci file ([0f26a59](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/0f26a590d21345dddec70b56ee74a13caa1a50ac))
* **ci:** update ci file ([843ea51](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/843ea519dc035b40d09808608e3bc8a7759d0ec7))


### Features

* **main:** add multiply api ([8cb2856](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/8cb2856c3a9a459dd8e019a56fc9176aa3902998))
* **tests:** add more tests ([ad34cc7](https://gitlab.com/m.ragab6010/gitops-web-app-example/commit/ad34cc7b026e53333a21c6aa386f8b70e23cd70d))
